# INF3135 Laboratoires
par Eric Pietrocupo

Ceci est une série d'exercice dans le but de vous faire expérimenter plusieurs cas de figure avec les pointeurs. Certains cas ne seront pas couvert par ces exercices. Les solutions sont incluse sur cette page, donc vous pouvez vous corriger. Des exercices seront ajoutés plus tard si le temps me le permet. Je vous recommande très fortement de faire un dessin pour suivre l'évolution de chaque étape. Même moi je me suis trompé pendant la conception de cet exercice.

## Représentation de la mémoire

__Pile(Stack)__: Mémoire qui accumule les variables sous forme de pile (FILO: First In Last Out). Donc les éléments sont retirés en ordre inverse d'ajout. Pour les besoins de l'exercice, les adresses mémoire de la pile commenceront à partir de 00 en montant. Il n'y aura pas non plus d'opérations de dépilement à effectuer. Les variables locales et tableaux statiques seront alloués sur la pile.

Tas(Heap): Mémoire qui est allouée au besoin du programme avec les fonctions de la famille alloc(malloc, calloc, realloc, etc). Pour les besoins de l'exercice, les allocations seront fait en ordre à partir de 99 en descendant. 

Case mémoire: Pour les besoins de l'exercice, une case mémoire sera numéroté de 00 à 99. Elle contient chacun 1 élément: variable, structure, élément de tableau, etc. Donc on ne se préoccupe pas du nombre d'octets utilisé. `nl` sera utilisé pour représenter la valeur NULL, on assume que toute mémoire est initialisée à `nl`.

Pointeurs: On peut représenter les pointeurs visuellement avec une flèche vers une cible. Mais dans le cadre de cet exercice, un pointeur contiendra l'adresse mémoire de la cible. 


# Exercice 1: Pointeurs uniques

Dans ce premier exercice on utilise des pointeurs qui ne font références qu'à une seule variable. Les tableaux et les structures seront vu dans les exercices subséquents. Analyser le code ci-dessous et déterminez l'état de la mémoire après chaque commentaire identifié "état de la mémoire". Ensuite, indiquez ce que cette fonction affichera à l'exécution.

~~~
void pointeurs_uniques ( void )
{	
	int a = 23;
	int *p1 = &a;
	//etat de la memoire 01
	int b = 36;
	int c = 53;
	*p1 = 42;
	p1 = &b;
	c = *p1;
	//etat de la memoire 02
	int *p2 = &a;
	int **p3 = &p1;
	**p3 = 65;
	p3 = &p2;
	**p3 = 51;
	//etat de la memoire 03
	p1 = (int*)malloc(sizeof(int));
	*p1 = 28;
	p2 = (int*)malloc(sizeof(int));
	*p2 = c + 3;
	//etat de la memoire 04

    //afficher le contenu des variables
	printf ("Pointeurs uniques\n");
	printf("int a          = %d\n", a);
	printf("int b          = %d\n", b);
	printf("int c          = %d\n", c);
	printf("ptr int p1     = %d\n", *p1);
	printf("ptr int p2     = %d\n", *p2);
	printf("ptr ptr int p3 = %d\n", **p3);
	free(p1);
	free(p2);	
}

~~~

État de la memoire 01
~~~
Pile(Stack)			Tas(Heap)
00 [23] a			99 [nl] <-Tas	
01 [00] p1
02 [nl] <- Pile
~~~


<details>
<summary>Solution: État de la memoire 02</summary>

~~~
Pile(Stack)			Tas(Heap)
00 [42] a			99 [nl] <-Tas	
01 [02] p1
02 [36] b 
03 [36] c
04 [nl] <- Pile
~~~

</details>

<details>
<summary>Solution: État de la memoire 03</summary>

~~~
Pile(Stack)			Tas(Heap)
00 [51] a			99 [nl] <-Tas	
01 [02] p1
02 [65] b 
03 [36] c
04 [00] p2
05 [04] p3
06 [nl] <- Pile
~~~

</details>

<details>
<summary>Solution: État de la memoire 04</summary>

~~~
Pile(Stack)			Tas(Heap)
00 [51] a			99 [28]	
01 [99] p1          98 [39]
02 [65] b           97 [nl] <-Tas
03 [36] c
04 [98] p2
05 [04] p3
06 [nl] <- Pile
~~~

</details>

<details>
<summary>Solution: Affichage à l'écran</summary>

~~~
Pointeurs uniques
int a          = 51
int b          = 65
int c          = 36
ptr int p1     = 28
ptr int p2     = 39
ptr ptr int p3 = 39
~~~

</details>

