/*  Exercice: Cauchemard de pointeurs
	@author Eric Pietrocupo
*/
#include <stdio.h>
#include <stdlib.h> // pour malloc() et free()

// Structures d'un entier statique
typedef struct EntierSt 
{	int valeur;
} EntierSt;

// Structure d'un entier dynamique
typedef struct EntierDn
{	int *ptr_valeur;
} EntierDn;

void pointeurs_uniques ( void )
{	
	int a = 23;
	int *p1 = &a;
	//etat de la memoire 01
	int b = 36;
	int c = 53;
	*p1 = 42;
	p1 = &b;
	c = *p1;
	//etat de la memoire 02
	int *p2 = &a;
	int **p3 = &p1;
	**p3 = 65;
	p3 = &p2;
	**p3 = 51;
	//etat de la memoire 03
	p1 = (int*)malloc(sizeof(int));
	*p1 = 28;
	p2 = (int*)malloc(sizeof(int));
	*p2 = c + 3;
	//etat de la memoire 04

    //afficher le contenu des variables
	printf ("Pointeurs uniques\n");
	printf("int a          = %d\n", a);
	printf("int b          = %d\n", b);
	printf("int c          = %d\n", c);
	printf("ptr int p1     = %d\n", *p1);
	printf("ptr int p2     = %d\n", *p2);
	printf("ptr ptr int p3 = %d\n", **p3);
	free(p1);
	free(p2);	
}

void tableaux ( void )
{
}

void structures ( void )
{
}

// programme principal
int main ( void )
{	printf ("Cauchemard de pointeurs\n");	
	pointeurs_uniques ();
	tableaux();
	structures();
	return 0;
}
